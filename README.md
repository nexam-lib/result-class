## Result

A class that for transparent error propagation and handling similar to Rusts result

You can do computations with values without having to check for validity with the `map`-method.
```dart
   final r = const Result<int, String>.ok(2);
   final r1 = r.map((value) => value * 2);
   assert(r1.contains(4));
```

It's also possible to `flatMap` a function that returns a `Result` over the value to prevent nesting of [Result]s
```dart
   Result<int, String> failable(int i) => i > 5
      ? const Result.err("Some error happened")
      : Result.ok(i * 2);
   
   final s = const Result<int, String>.ok(10);
   final s1 = s.flatMap(failable);
   assert(s1.containsErr("Some error happened"));
```

`mapErr` and `flatMapErr` provide equivalent functionality for errors.
