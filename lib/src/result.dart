/// [Result<T, E>] is a type that represents either success or failure
///
/// It can be used to transparently return results from functions than can
/// possibly fail, whereas exceptions have to be documented manually.
class Result<T, E> {
  /// Creates a successful result from [value]
  const Result.ok(T value)
      : _v = value,
        isOk = true;

  /// Creates a failed result from [error]
  const Result.err(E error)
      : _v = error,
        isOk = false;

  final dynamic _v;

  T get _value => _v as T;
  E get _error => _v as E;

  /// Whether this is a succesful result
  final bool isOk;

  /// Whether this is a failed result
  bool get isErr => !isOk;

  /// Returns the value if [isOk] is true, otherwise returns null
  T? get valueOrNull => isOk ? _value : null;

  /// Returns the error if [isOErr] is true, otherwise returns null
  E? get errorOrNull => isErr ? _error : null;

  /// Returns the value if [isOk] is true, otherwise returns [defaultValue]
  T valueOr(T defaultValue) => isOk ? _value! : defaultValue;

  /// Returns the error if [isOErr] is true, otherwise returns [defaultError]
  E errorOr(E defaultError) => isErr ? _error! : defaultError;

  /// Returns true is [isOk] is true and if [value] equals value, otherwise returns
  /// false
  bool contains(T value) => isOk && _value == value;

  /// Returns true if [isErr] is true and if [error] equals error, otherwise
  /// returns false
  bool containsErr(E error) => isErr && _error == error;

  /// Maps the value of a successful [Result<T, E>] from [T] to [U], leaving an
  /// error untouched.
  Result<U, E> map<U>(U Function(T) f) =>
      isOk ? Result.ok(f(_value)) : Result.err(_error);

  /// Maps [Result<T, E>] to [Result<T, F>], leaving an [Ok] value untouched.
  Result<T, F> mapErr<F>(F Function(E) f) =>
      isOk ? Result.ok(_value) : Result.err(f(_error));

  /// Maps [Result<T, E>] to [Result<U, E>], leaving an [Err] value untouched.
  ///
  /// Allows chaining [Result]s without nesting them.
  Result<U, E> flatMap<U>(Result<U, E> Function(T) f) =>
      isOk ? f(_value) : Result.err(_error);

  /// Maps a [Result<T, E>] to [U] by applying [ok] to a contained value or
  /// [err] to a contained error.
  U mapOrElse<U>(U Function(T) ok, U Function(E) err) =>
      isOk ? ok(_value) : err(_error);

  /// Maps [Result<T, E>] to [Result<U, E>], leaving an [Err] value untouched.
  ///
  /// Allows chaining [Result]s without nesting them.
  Result<U, F> flatMapOrElse<U, F>(
    Result<U, F> Function(T) f,
    Result<U, F> Function(E) e,
  ) =>
      isOk ? f(_value) : e(_error);

  /// Returns a [Result<U, F>] where value/error is cast to the corresponding type.
  ///
  /// If casting fails, throws a TypeError.
  Result<U, F> cast<U, F>() =>
      isOk ? Result<U, F>.ok(_value as U) : Result<U, F>.err(_error as F);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    if (other is! Result<T, E>) return false;

    if (runtimeType != other.runtimeType) return false;

    if (isOk && other.isOk) {
      return _value == other._value;
    } else {
      if (isErr && other.isErr) return _error == other._error;
    }

    return false;
  }

  @override
  int get hashCode => isOk.hashCode ^ _v.hashCode;

  @override
  String toString() =>
      isOk ? 'Result<$T, $E>.ok($_value)' : 'Result<$T, $E>.err($_value)';
}
