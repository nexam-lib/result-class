import 'package:result_class/result_class.dart';

void main() {
  final r = const Result<int, String>.ok(2);
  final r1 = r.map((value) => value * 2);
  assert(r1.contains(4));

  Result<int, String> failable(int i) =>
      i > 5 ? const Result.err('Some error happened') : Result.ok(i * 2);

  final s = const Result<int, String>.ok(10);
  final s1 = s.flatMap(failable);
  assert(s1.containsErr('Some error happened'));
}
