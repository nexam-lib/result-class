import 'package:test/test.dart';

import 'package:result_class/src/result.dart';

class _Error {
  const _Error(this.b);

  final bool b;

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is _Error && o.b == b;
  }

  @override
  int get hashCode => b.hashCode;
}

void main() {
  group('Result', () {
    final successValue = 2;
    final success = Result<int, _Error>.ok(successValue);
    final failureError = const _Error(false);
    final failure = Result<int, _Error>.err(failureError);

    test('==', () {
      final intErrRes = const Result<int, _Error>.ok(2);

      expect(intErrRes, const Result<int, _Error>.ok(2));
      expect(intErrRes, isNot(const Result<int, dynamic>.ok(2)));
      expect(intErrRes, isNot(Result<dynamic, _Error>.err(failureError)));

      final intErrRes1 = const Result<int, _Error>.err(_Error(true));
      expect(intErrRes, isNot(intErrRes1));
      expect(intErrRes1, isNot(const Result<int, _Error>.err(_Error(false))));
    });

    test('value and error getters', () {
      expect(success.valueOrNull, 2);
      expect(success.valueOr(5), 2);
      expect(success.errorOrNull, null);
      expect(success.errorOr(_Error(!failureError.b)), _Error(!failureError.b));

      expect(failure.valueOrNull, null);
      expect(failure.valueOr(5), 5);
      expect(failure.errorOrNull, failureError);
      expect(failure.errorOr(_Error(!failureError.b)), failureError);
    });

    test('.map', () {
      int square(int x) => x * x;

      expect(success.map(square), Result<int, _Error>.ok(square(successValue)));
      expect(failure.map(square), Result<int, _Error>.err(failureError));
    });

    test('.mapErr', () {
      _Error invert(_Error e) => _Error(!e.b);

      expect(success.mapErr(invert), Result<int, _Error>.ok(successValue));
      expect(failure.mapErr(invert),
          Result<int, _Error>.err(invert(failureError)));
    });

    test('.flatMap', () {
      Result<int, _Error> succeedesIdentical(int i) => Result.ok(i);
      Result<int, _Error> fails(int i) => failure;

      expect(success.flatMap(succeedesIdentical), success);
      expect(success.flatMap(fails), failure);

      expect(failure.flatMap(succeedesIdentical), failure);
      expect(failure.flatMap(fails), failure);
    });

    test('.match', () {
      expect(success.mapOrElse((value) => 5, (error) => 5), 5);
      expect(failure.mapOrElse((value) => 5, (error) => 5), 5);
    });

    test('.cast', () {
      expect(const Result<num, dynamic>.ok(1).cast<int, dynamic>(),
          const Result<int, dynamic>.ok(1));
      expect(const Result<dynamic, num>.err(1.5).cast<dynamic, double>(),
          const Result<dynamic, double>.err(1.5));

      expect(
        () => const Result<int, dynamic>.ok(1).cast<String, double>(),
        throwsA(isA<TypeError>()),
      );
    });
  });
}
