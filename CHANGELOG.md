## 1.0.0

- Add example and update README
- Format files

## 1.0.0-nullsafety.1

- Add links to repository and issue tracker to pubspec.yaml

## 1.0.0-nullsafety

- Initial version
